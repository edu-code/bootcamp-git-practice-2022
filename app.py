from flask import Flask


def create_app():
    app = Flask(__name__)

    @app.route('/status')
    def healthcheck():
        return 'OK'

    @app.route('/red00')
    def red00():
        return '00'

    @app.route('/red05')
    def red05():
        return '05'

    @app.route('/red48')
    def red48():
        return '48'

    @app.route('/red13')
    def red13():
        return '13'

    @app.route('/red17')
    def red17():
        return '17'
    
    @app.route('/red01')
    def red00():
        return '01'

    return app

app = create_app()

