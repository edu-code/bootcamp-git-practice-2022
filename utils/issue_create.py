import gitlab

url = 'https://gitlab.com'
project_id = 34947841
private_token = 'xxxx'  # nosec B105
test = True

# private token or personal token authentication (GitLab.com)
gl = gitlab.Gitlab(url, private_token)

# Get a project by ID
project = gl.projects.get(project_id)


for i in range(50):
    if not test:
        # Create issue
        issue = project.issues.create({'title': f'''Добавить в приложение route /red{i:02d}''',
                                      'description': f'''Добавить в приложение route `/red{i:02d}`, ''' +
                                       f'''возращающий {i:02d}.'''})
    else:
        print({'title': f'''Добавить в приложение route /red{i:02d}''',
              'description': f'''Добавить в приложение route `/red{i:02d}`, возращающий {i:02d}.'''})
