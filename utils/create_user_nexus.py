import csv
import requests
import logging
import pprint
import json
import urllib.parse

from requests.auth import HTTPBasicAuth

logging.basicConfig(format='%(asctime)s %(levelname)s : %(message)s [in %(funcName)s %(pathname)s:%(lineno)d]', level=logging.DEBUG)
logger = logging.getLogger(__name__)

pp = pprint.PrettyPrinter(indent=2)

url = 'http://nexus.com:9095/'
username = 'admin'  # nosec B105
password = 'xxxx'  # nosec B105
test = True

with open('users.csv', 'r', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
    for row in spamreader:
        user_name = row[0]
        user_pass = row[1]
        user_object = { "userId": user_name,
                        'firstName': f'First {user_name}',
                        'lastName': f'Last {user_name}',
                        'emailAddress': f'{user_name}@example.com',
                        'password': user_pass,
                        'status': 'active',
                        'roles': ['nx-cicd'],
                        }
        if not test:
            headers = {'accept': 'application/json', 'Content-Type': 'application/json'}
            basic = HTTPBasicAuth(username, password)
            request_url = urllib.parse.urljoin(url, 'service/rest/v1/security/users')
            r = requests.post(request_url, data=json.dumps(user_object), auth=basic, headers=headers)
            logger.debug(r.text)
        else:
            print(user_object)
