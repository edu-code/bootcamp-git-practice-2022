import gitlab
import csv

url = 'https://gitlab.com'
private_token = 'xxxx'  # nosec B105
test = False

# private token or personal token authentication (GitLab.com)
gl = gitlab.Gitlab(url, private_token)

with open('users.csv', 'r', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
    for row in spamreader:
        user_name = row[0]
        password = row[1]
        user_object = {'email': f'{user_name}@example.com',
                        'password': password,
                        'username': user_name,
                        'name': user_name,
                        'skip_confirmation': True}
        if not test:
            # Create user
            #user = gl.users.create(user_object)
            user = gl.users.list(username=user_name)[0]
            user.confirmed_at = user.created_at
            user.activate()
            user.save()
        else:
            user = gl.users.list(username=user_name)[0]
            #user.skip_confirmation = True
            print(user)
