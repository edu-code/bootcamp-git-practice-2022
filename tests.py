#! python
import unittest
from app import create_app


class UserModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    def test_status_route(self):
        res = self.client().get('/status')
        self.assertEqual(res.status_code, 200)
        self.assertIn('OK', str(res.data))

    def test_red00_route(self):
        res = self.client().get('/red00')
        self.assertEqual(res.status_code, 200)
        self.assertIn('00', str(res.data))


if __name__ == "__main__":
    unittest.main()
